# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

## Pull Request Process

1. Ensure any install or build dependencies are removed before the end of the layer when doing a build.
2. Update the README.md with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters.
3. Increase the version numbers in any examples files and the README.md to the new version that this Pull Request would represent.
4. You may merge the Pull Request in once you have the sign-off of two other developers, or if you do not have permission to do that, you may request the second reviewer to merge it for you.

## Versioning

Releases get created by the maintainers of the repository. A release will be created after a substantial amount of contributions have been made or a severe bug has been fixed.  
The versioning scheme we use is [SemVer](http://semver.org/).  
All versions should include a changelog.  
There should be a dedicated commit to increase the version number. This commit will be marked as a release in GitLab and published to NPM.
